# vpn.mk16.de

This Ansible profile describes a VPN server based on VyOS. This has one IPv4 and one IPv6. Furthermore, it has a /64 subnet. It is connected to the dnet (dn42 + NeoNetwork + CRXN) via BGP with BFD.

Roles:
  - `basics` - Increases the commit revision, sets the system name and domain, removes the motd message, and enables system reboot on kernel panic.
  - `connection` - Configures DNS servers and the IP addresses and routes to access the Internet.
  - `dnet` - Configures the connection between the dnet gateways and sets the appropriate settings.
  - `dns-forwarding` - Activates a DNS forwarding service to the system DNS servers and enables it for the clients.
  - `firewall` - Configures the firewall. It is especially important that no firewall rule number is used twice. Because of the rule numbering, all firewall settings are made in this role.
  - `network-tweaks` - Increases some kernel limits for better performance.
  - `roadwarriar-vpn` - Core module for the VPN server. Sets up the WireGuard clients and associated rules. Without this module, the `dnet` module will not work.
  - `save-configuration` - Saves the current configuration. (This could also be outsourced to a handler).
  - `ssh` - Enables SSH
  - `ssh-ddos` - Enables SSH brute force protection, similar to fail2ban.
  - `ssh-keys` - Installs my SSH keys
  - `ssh-secure` - Limits the use of the algorithms with SSH to a safe minimum (only chacha20 with poly1305, AES256-GCM, no Diffle-Hellman, only Curve25519-SHA256, HMAC-sha2-512, umac-128) Maybe disable umac?!
