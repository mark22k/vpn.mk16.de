
# Firewall documentation

| Rule range | Usage |
| --- | --- |
| `10` | stateful |
| `20-300` | ICMP |
| `400` | UDP traceroute |
| `500-999` | Custom rules (SSH, DNS, ...) |
| `1000-1099` | dn-connect BGP rules |
| `1100-1199` | dn-connect BFD rules |
| `1200-1299` | dn-connect WireGuard |

## Maintenance firewall

The maintenance firewall has a special task: it should be minimal and active while the firewall is reconfigured.
To reconfigure the firewall, the first thing to do is to delete it and thus disable it. However, in the time between the deletion and the transfer of the new firewall data, there is a security gap. The maintenance firewall fixes this problem. It only allows SSH, no ping or other. Deleting the firewall and creating and activating the maintenance firewall is done in a single step, so the router is never without a firewall.
