---
- name: Setup BGP
  vyos_config:
    lines:
      - "set protocols bgp system-as {{ dn_routing.ownas }}"
      - "set protocols bgp address-family ipv4-unicast network {{ dn_routing.ownnet.ipv4 }}"
      - "set protocols bgp address-family ipv6-unicast network {{ dn_routing.ownnet.ipv6 }}"
      - "set protocols bgp parameters router-id {{ dn_routing.ownip.ipv4 }}"
      - "set protocols bgp listen limit 20"
      - "set protocols bgp parameters bestpath med missing-as-worst"

- name: Setup wireguard interfaces
  vyos_config:
    lines:
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} address {{ item.tunnel.ipv6 }}"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} port {{ item.port }}"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} private-key {{ item.priv }}"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} peer dnpeer allowed-ips ::/0"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} peer dnpeer allowed-ips 0.0.0.0/0"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} peer dnpeer public-key {{ item.peer.pub }}"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} peer dnpeer preshared-key {{ item.peer.psk }}"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} peer dnpeer address {{ item.peer.endpoint }}"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} peer dnpeer port {{ item.peer.port }}"
      - "set interfaces wireguard wg{{ item.port - 40000 + 10 }} description 'wg tunnel {{ item.name }}'"
  with_items:
    - "{{ dn_servers }}"

- name: Setup unreachable for own prefixes
  vyos_config:
    lines:
      - "set protocols static route {{ dn_routing.ownnet.ipv4 }} reject"
      - "set protocols static route6 {{ dn_routing.ownnet.ipv6 }} reject"

- name: Setup unreachable for dnet prefixes
  vyos_config:
    lines:
      - "set protocols static route 172.16.0.0/12 reject"
      - "set protocols static route 10.0.0.0/8 reject"
      - "set protocols static route6 fd00::/8 reject"

- name: Setup dummy interface
  vyos_config:
    lines:
      - "set interfaces dummy dum1 address {{ dn_routing.ownip.ipv4 }}/32"
      - "set interfaces dummy dum1 address {{ dn_routing.ownip.ipv6 }}/128"
      - "set interfaces dummy dum1 description 'dnet dummy interface'"

- name: Create prefix lists
  vyos_config:
    lines:
      # Prefix list IPv6
      - "set policy prefix-list6 dnet"
      - "set policy prefix-list6 dnet rule 10 action permit"
      - "set policy prefix-list6 dnet rule 10 prefix fd00::/8"
      - "set policy prefix-list6 dnet rule 10 ge 44"
      - "set policy prefix-list6 dnet rule 10 le 128"
      - "set policy prefix-list6 dnet rule 10 description 'ULA range'"
      ## default action
      - "set policy prefix-list6 dnet rule 65535 action deny"
      - "set policy prefix-list6 dnet rule 65535 prefix ::/0"
      # Prefix list IPv4
      - "set policy prefix-list dnet"
      - "set policy prefix-list dnet rule 10 action permit"
      - "set policy prefix-list dnet rule 10 prefix 172.20.0.0/14"
      - "set policy prefix-list dnet rule 10 le 29"
      - "set policy prefix-list dnet rule 10 ge 21"
      - "set policy prefix-list dnet rule 10 description 'dnet'"
      - "set policy prefix-list dnet rule 20 action permit"
      - "set policy prefix-list dnet rule 20 prefix 172.20.0.0/24"
      - "set policy prefix-list dnet rule 20 le 32"
      - "set policy prefix-list dnet rule 20 ge 28"
      - "set policy prefix-list dnet rule 20 description 'dnet anycast'"
      - "set policy prefix-list dnet rule 30 action permit"
      - "set policy prefix-list dnet rule 30 prefix 172.21.0.0/24"
      - "set policy prefix-list dnet rule 30 le 32"
      - "set policy prefix-list dnet rule 30 ge 28"
      - "set policy prefix-list dnet rule 30 description 'dnet anycast'"
      - "set policy prefix-list dnet rule 40 action permit"
      - "set policy prefix-list dnet rule 40 prefix 172.22.0.0/24"
      - "set policy prefix-list dnet rule 40 le 32"
      - "set policy prefix-list dnet rule 40 ge 28"
      - "set policy prefix-list dnet rule 40 description 'dnet anycast'"
      - "set policy prefix-list dnet rule 50 action permit"
      - "set policy prefix-list dnet rule 50 prefix 172.23.0.0/24"
      - "set policy prefix-list dnet rule 50 le 32"
      - "set policy prefix-list dnet rule 50 ge 28"
      - "set policy prefix-list dnet rule 50 description 'dnet anycast'"
      - "set policy prefix-list dnet rule 60 action permit"
      - "set policy prefix-list dnet rule 60 prefix 172.31.0.0/16"
      - "set policy prefix-list dnet rule 60 description 'ChaosVPN Eurpoean area'"
      - "set policy prefix-list dnet rule 70 action permit"
      - "set policy prefix-list dnet rule 70 prefix 10.100.0.0/14"
      - "set policy prefix-list dnet rule 70 description 'ChaosVPN USA area'"
      - "set policy prefix-list dnet rule 90 action permit"
      - "set policy prefix-list dnet rule 90 prefix 10.0.0.0/8"
      - "set policy prefix-list dnet rule 90 le 24"
      - "set policy prefix-list dnet rule 90 ge 15"
      - "set policy prefix-list dnet rule 90 description 'ICVPN / Freifunk'"
      # default action
      - "set policy prefix-list dnet rule 65535 action deny"
      - "set policy prefix-list dnet rule 65535 prefix 0.0.0.0/0"

- name: Setup BGP peers
  vyos_config:
    lines:
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} remote-as 4242422923"
      # external does not work with bogen ASNs
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} description '{{ item.name }}'"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} capability dynamic"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} capability extended-nexthop"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} address-family ipv4-unicast maximum-prefix 2000"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} address-family ipv6-unicast maximum-prefix 2000"
      #- "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} bfd"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} address-family ipv4-unicast prefix-list import dnet"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} address-family ipv6-unicast prefix-list import dnet"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} address-family ipv4-unicast soft-reconfiguration inbound"
      - "set protocols bgp neighbor {{ item.tunnel.peer.ipv6 }} address-family ipv6-unicast soft-reconfiguration inbound"
  with_items:
    - "{{ dn_servers }}"

- name: Setup NAT4
  vyos_config:
    lines:
      - "set nat source rule 20 source address 192.168.42.0/24"
      - "set nat source rule 20 translation address '{{ dn_routing.clients.ipv4 }}'"
      # temporary hack to let vyos make a commit
      - "set nat source rule 20 outbound-interface lo"

- name: Add interfaces to NAT4
  vyos_config:
    lines:
      - "set nat source rule 20 outbound-interface wg{{ item.port - 40000 + 10 }}"
  with_items:
    - "{{ dn_servers }}"

- name: Cleaning up
  vyos_config:
    lines:
      - "delete nat source rule 20 outbound-interface lo"

- name: Setup NAT6
  vyos_config:
    lines:
      - "set nat66 source rule 20 source prefix '{{ roadwarriar.ipv6 }}'"
      - "set nat66 source rule 20 translation address '{{ dn_routing.clients.ipv6 }}'"
      # temporary hack to let vyos make a commit
      - "set nat66 source rule 20 outbound-interface lo"

- name: Add interfaces to NAT6
  vyos_config:
    lines:
      - "set nat66 source rule 20 outbound-interface wg{{ item.port - 40000 + 10 }}"
  with_items:
    - "{{ dn_servers }}"

- name: Cleaning up
  vyos_config:
    lines:
      - "delete nat66 source rule 20 outbound-interface lo"
